﻿using System.Collections;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject enemy;
    public int totalSpawnByType = 10;
    public float spawnTimeSeconds = 5f;
    private SpriteRenderer sprite;
    private bool disabling;
    private int totalEnemies;

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        disabling = false;
        InvokeRepeating("Spawn", spawnTimeSeconds, spawnTimeSeconds);
    }

    void Update()
    {
        transform.Rotate(Vector3.forward * -90 * Time.deltaTime);

        totalEnemies = GameObject.FindGameObjectsWithTag(enemy.tag).Length;

        if (totalEnemies <= 0)
        {
            StartCoroutine(FadeTo(0, 5));
        }
    }

    void Spawn()
    {
        if (!disabling && totalEnemies < totalSpawnByType)
        {
            Instantiate(enemy, transform.position, Quaternion.identity);
        }
    }

    IEnumerator FadeTo(float aValue, float aTime)
    {
        disabling = true;
        Color color = sprite.color;
        float alpha = color.a;
        for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
        {
            Color newColor = new Color(color.r, color.g, color.b, Mathf.Lerp(alpha, aValue, t));
            sprite.color = newColor;
            yield return null;
        }
        transform.gameObject.SetActive(false);
    }
}
