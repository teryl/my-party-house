﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float danceMoveSpeed = 10f;

    [HideInInspector]
    public float timeBetweenMove = 0.05f;

    [HideInInspector]
    public float timeToMove = 0.125f;

    private Rigidbody2D myRigidbody;
    private Vector3 moveDirection;
    private GameObject thePlayer;

    private bool moving;
    private float timeBetweenMoveCounter;
    private float timeToMoveCounter;

    private bool isFollow;

    [HideInInspector]
    public float minDistance = 2f;
    public float maxFollowDistance = 5f;
    public float followSpeedMultiplier = 0.9f;

    public EnemyType.Type enemyType = EnemyType.Type.A;

    private Transform target;
    private PlayerController player;

    public AudioClip[] audioClips;
    private AudioSource theAudio;
    private bool soundPlayed;
    private SpriteRenderer sprite;

    void Start()
    {
        myRigidbody = GetComponent<Rigidbody2D>();
        timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
        timeToMoveCounter = Random.Range(timeToMove * 0.75f, timeToMove * 1.25f);

        player = FindObjectOfType<PlayerController>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        theAudio = GetComponent<AudioSource>();
        sprite = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        var distanceToPlayer = Vector2.Distance(transform.position, target.position);

        // too close, stop moving
        if (distanceToPlayer <= minDistance)
        {
            myRigidbody.velocity = Vector2.zero;
            return;
        }

        // follow player
        isFollow = player.maskType == enemyType && distanceToPlayer > minDistance && distanceToPlayer <= maxFollowDistance;

        if (isFollow)
        {
            if (!soundPlayed)
            {
                theAudio.clip = audioClips[Random.Range(0, audioClips.Length)];
                theAudio.Play();
                soundPlayed = true;
            }

            sprite.color = new Color(1, 1, 1, 0.5f);
            transform.position = Vector2.MoveTowards(transform.position, target.position, player.speed * followSpeedMultiplier * Time.deltaTime);
            return;
        }
        else
        {
            sprite.color = Color.white;
            soundPlayed = false;
        }

        // normal party move
        NormalMove();
    }

    private void NormalMove()
    {
        if (moving)
        {
            timeToMoveCounter -= Time.deltaTime;
            myRigidbody.velocity = moveDirection;

            if (timeToMoveCounter < 0f)
            {
                moving = false;
                timeBetweenMoveCounter = Random.Range(timeBetweenMove * 0.75f, timeBetweenMove * 1.25f);
            }
        }
        else
        {
            timeBetweenMoveCounter -= Time.deltaTime;
            myRigidbody.velocity = Vector2.zero;

            if (timeBetweenMoveCounter < 0f)
            {
                moving = true;
                timeToMoveCounter = Random.Range(timeToMove * 0.75f, timeToMove * 1.25f);
                moveDirection = new Vector3(Random.Range(-1f, 1f) * danceMoveSpeed, Random.Range(-1f, 1f) * danceMoveSpeed, 0f);
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag.Equals("Player"))
        {
            theAudio.clip = audioClips[Random.Range(0, audioClips.Length)];
            theAudio.Play();
        }
    }
}
