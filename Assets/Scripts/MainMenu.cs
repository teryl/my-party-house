﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject howToPanel;
    public GameObject mainMenuPanel;
    public GameObject exitBtn;

    void Start()
    {
        Time.timeScale = 1;

        if (Application.platform == RuntimePlatform.WebGLPlayer)
        {
            exitBtn.SetActive(false);
        }
    }

    public void OnPlayPressed(string levelToLoad)
    {
        SceneManager.LoadScene(levelToLoad);
    }

    public void OnExitPressed()
    {
        Application.Quit();
    }

    public void OnHowToPressed()
    {
        howToPanel.SetActive(true);
        mainMenuPanel.SetActive(false);
    }

    private void Update()
    {
        if (howToPanel.activeInHierarchy && (Input.GetKeyDown(KeyCode.Escape) || Input.GetMouseButtonDown(0)))
        {
            howToPanel.SetActive(false);
            mainMenuPanel.SetActive(true);
        }
    }
}
