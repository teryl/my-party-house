﻿using System.Collections;
using System.Security.Permissions;
using UnityEngine;

public class Mask : MonoBehaviour
{
    public EnemyType.Type enemyType = EnemyType.Type.A;

    private PlayerController player;

    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            DisableMask();
            player.SwitchMask(enemyType);
        }
    }

    public void DisableMask()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<Collider2D>().enabled = false;
    }

    public void EnableMask()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<Collider2D>().enabled = true;
    }
}
