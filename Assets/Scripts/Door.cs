﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private List<string> enemyTags = new List<string> { "EnemyA", "EnemyB", "EnemyC" };

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (enemyTags.Exists(x => string.Equals(x, collision.transform.tag, StringComparison.OrdinalIgnoreCase)))
        {
            Destroy(collision.gameObject);
        }
    }
}