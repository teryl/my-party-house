﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject PauseMenuPanel;
    public GameObject PauseButtonPanel;

    private Music music;

    void Start()
    {
        music = FindObjectOfType<Music>();
    }

    void Update()
    {
        if (Application.platform != RuntimePlatform.WebGLPlayer && Input.GetKeyDown(KeyCode.Escape))
        {
            if (PauseMenuPanel.activeInHierarchy)
            {
                HideMenu();
            }
            else
            {
                ShowMenu();
            }
        }

        if (PauseMenuPanel.activeInHierarchy)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;
        }
    }

    public void OnPauseClicked()
    {
        ShowMenu();
    }

    public void OnContinuePressed()
    {
        HideMenu();
    }

    public void OnRestartPressed()
    {
        HideMenu();
        FindObjectOfType<GameManager>().ResetLevel();
    }

    public void OnMainMenuPressed()
    {
        HideMenu();
        SceneManager.LoadScene("Main Menu");
    }

    private void ShowMenu()
    {
        PauseMenuPanel.SetActive(true);
        PauseButtonPanel.SetActive(false);
        music.paused = true;
        music.GetComponent<AudioSource>().volume *= 0.15f;
    }

    private void HideMenu()
    {
        PauseMenuPanel.SetActive(false);
        PauseButtonPanel.SetActive(true);
        music.paused = false;
    }
}
