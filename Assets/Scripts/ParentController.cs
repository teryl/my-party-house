﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParentController : MonoBehaviour
{

    private Transform target;
    private PlayerController player;

    private float minDistance = 2f;
    public float followSpeedMultiplier = 0.9f;

    // Start is called before the first frame update
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        var distanceToPlayer = Vector2.Distance(transform.position, target.position);

        if (distanceToPlayer > minDistance) {
            transform.position = Vector2.MoveTowards(transform.position, target.position, player.speed * followSpeedMultiplier * Time.deltaTime);
        }
    }
}
