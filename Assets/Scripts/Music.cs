﻿using UnityEngine;

public class Music : MonoBehaviour
{
    private float totalEnemies;

    private AudioSource theAudio;
    public bool paused;

    void Start()
    {
        totalEnemies = FindObjectsOfType<EnemyController>().Length;

        theAudio = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (!paused)
        {
            var currentEnemies = FindObjectsOfType<EnemyController>().Length;
            float volume = currentEnemies / totalEnemies;
            theAudio.volume = volume;
        }
    }
}
