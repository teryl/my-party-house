﻿using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class PlayerController : MonoBehaviour
{
    public float speed;

    Rigidbody2D rb;

    public EnemyType.Type maskType;
    SpriteRenderer sprite;

    public Transform maskA;
    public Transform maskB;
    public Transform maskC;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponent<SpriteRenderer>();

        var theCamera = FindObjectOfType<CameraController>();
        if (theCamera != null) {
            theCamera.transform.position = new Vector3(transform.position.x, transform.position.y, theCamera.transform.position.z);
        }
    }

    void Update()
    {
        var enemies = FindObjectsOfType<EnemyController>();
        if (enemies.Length <= 0)
        {
            Reset();
            GameManager gm = FindObjectOfType<GameManager>();
            if (gm != null) {
                gm.LoadLevel();
            }
        }
    }

    void Reset()
    {
        SwitchMask(EnemyType.Type.None);
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxisRaw("Horizontal");
        float moveVertical = Input.GetAxisRaw("Vertical");

        rb.velocity = new Vector2(moveHorizontal * speed, moveVertical * speed);
    }

    public void SwitchMask(EnemyType.Type enemyType)
    {
        // enable old mask
        if (maskType != EnemyType.Type.None)
        {
            var masks = FindObjectsOfType<Mask>();
            foreach (var mask in masks)
            {
                if (mask.enemyType == maskType)
                {
                    mask.EnableMask();
                    break;
                }
            }
        }

        // equip new mask
        maskType = enemyType;

        if (maskType == EnemyType.Type.None)
        {
            sprite.color = Color.white;
        }
        else
        {
            sprite.color = GetMaskPrefab(maskType).GetComponent<SpriteRenderer>().color;
        }
    }

    private Transform GetMaskPrefab(EnemyType.Type type)
    {
        switch (type)
        {
            case EnemyType.Type.A:
                return maskA;

            case EnemyType.Type.B:
                return maskB;

            case EnemyType.Type.C:
                return maskC;

            case EnemyType.Type.None:
            default:
                return null;
        }
    }
}
