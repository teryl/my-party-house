﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public string currentSceneName;
    public string nextSceneName;

    public void SetLevel(string sceneName)
    {
        currentSceneName = sceneName;
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene(currentSceneName);
    }

    public void LoadLevel()
    {
        SetLevel(nextSceneName);
        SceneManager.LoadScene(nextSceneName);
    }
}
